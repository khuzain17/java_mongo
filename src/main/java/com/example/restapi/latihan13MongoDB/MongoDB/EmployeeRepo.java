package com.example.restapi.latihan13MongoDB.MongoDB;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.example.restapi.latihan13MongoDB.MongoDB.Employee;

import java.util.List;

public interface EmployeeRepo extends MongoRepository<Employee, String> {
    List<Employee> findByNameContaining(String name);

//    List<Employee> findByStatus(String name);
}
