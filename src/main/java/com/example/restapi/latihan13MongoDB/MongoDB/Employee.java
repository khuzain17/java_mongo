package com.example.restapi.latihan13MongoDB.MongoDB;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

//import javax.persistence.Id;
import java.util.Date;
//
//@Setter
//@Getter
@Data
@Document(collection = "employee")
public class Employee {

    @Id
    private String id;

    private String name;

    private String sex;

    @JsonFormat(pattern="dd-MM-yyyy")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date birth_date;

    private String address;

    private Integer status;
}
