package com.example.restapi.latihan13MongoDB.MongoDB;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/synrgy/java-mongo")
public class EmployeeMongoController {
    @Autowired
    EmployeeRepo employeeRepo;

    @GetMapping("/hello")
    public String getHello() {
        return "hello world";
    }

    @PostMapping("/save")
    public ResponseEntity<Employee> insertEmployee(@RequestBody Employee employee) {
        Map map = new HashMap();
        try {
            Employee obj = employeeRepo.save(employee);
            return new ResponseEntity<>(obj, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") String id, @RequestBody Employee employee) {
        Optional<Employee> employeeOptional = employeeRepo.findById(id);

        if (employeeOptional.isPresent()) {
            Employee obj = employeeOptional.get();
            obj.setName(employee.getName());
            obj.setSex(employee.getSex());
            obj.setBirth_date(employee.getBirth_date());
            obj.setAddress(employee.getAddress());
            obj.setStatus(employee.getStatus());

            return new ResponseEntity<>(employeeRepo.save(obj), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<Employee>> getAllEmployee(@RequestParam(required = false) String name) {
//        Map map = new HashMap();
        try {
            List<Employee> employeeList = new ArrayList<Employee>();

            if (name == null) {
                employeeRepo.findAll().forEach(employeeList::add);
            } else {
                employeeRepo.findByNameContaining(name).forEach((employeeList::add));
            }

            if (employeeList.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(employeeList, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") String id) {
        try {
            Optional<Employee> employeeOptional = employeeRepo.findById(id);

            if (employeeOptional.isPresent()) {
                return new ResponseEntity<>(employeeOptional.get(), HttpStatus.OK);
            }
            return new ResponseEntity<>(employeeOptional.get(), HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteEmployee(@PathVariable("id") String id) {
        try {
            employeeRepo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

